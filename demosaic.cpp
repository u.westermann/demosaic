#include "libraw/libraw.h"
#include <fcntl.h>
#include <getopt.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>


static char* file_name;
static int bayer_pattern = LIBRAW_OPENBAYER_GBRG;
static unsigned int quality = 0;
static unsigned int black_level = 0;
static unsigned short width = 1024;
static unsigned short height = 1024;


static void print_usage(char* executable, FILE* stream)
{
    fprintf(stream, "Usage: %s [OPTION]... FILE\n", executable);
    fprintf(stream,
            "Demosaic image sensor data. FILE must contain an array of "
            "octets, one octet per pixel, ordered from left to right and top "
            "to bottom pixel.\n");
    fprintf(stream, "Options:\n");
    fprintf(stream, "  -h\t\t\tshow help\n");
    fprintf(stream,
            "  -p pattern \t\tbayer pattern (pattern: rg, bg, gr, gb "
            "(default=gb))\n");
    fprintf(stream,
            "  -q interpolation \t0=linear, 1=VNG, 2=PPG, 3=AHD, 4=DCB, "
            "11=DHT, 12=AAHD (default=0)\n");
    fprintf(stream, "  -b blacklevel \tblack level (default=0)\n");
    fprintf(stream, "  -x pixels \t\twidth in pixels (default=1024)\n");
    fprintf(stream, "  -y pixels \t\theight in pixels (default=1024)\n");
}


static int parse_arguments(int argc, char* argv[])
{
    int option_char = 0;

    while ((option_char = getopt(argc, argv, "hp:q:b:x:y:")) != -1)
    {
        switch (option_char)
        {
            case 'p':
                if (strncmp(optarg, "rg", 2) == 0)
                {
                    bayer_pattern = LIBRAW_OPENBAYER_RGGB;
                }
                else if (strncmp(optarg, "bg", 2) == 0)
                {
                    bayer_pattern = LIBRAW_OPENBAYER_BGGR;
                }
                else if (strncmp(optarg, "gr", 2) == 0)
                {
                    bayer_pattern = LIBRAW_OPENBAYER_GRBG;
                }
                else if (strncmp(optarg, "gb", 2) == 0)
                {
                    bayer_pattern = LIBRAW_OPENBAYER_GBRG;
                }
                else
                {
                    fprintf(stderr, "unknown bayer pattern\n");
                    print_usage(argv[0], stderr);
                }
                break;
            case 'q':
                quality = strtoul(optarg, NULL, 10);
                break;
            case 'b':
                black_level = strtoul(optarg, NULL, 10);
                break;
            case 'x':
                width = strtoul(optarg, NULL, 10);
                break;
            case 'y':
                height = strtoul(optarg, NULL, 10);
                break;
            case 'h':
                print_usage(argv[0], stdout);
                return -1;
            default:
                fprintf(stderr, "option incorrect\n");
                print_usage(argv[0], stderr);
                return -1;
        }
    }

    if (optind != argc - 1)
    {
        fprintf(stderr, "no file given\n");
        return -1;
    }

    file_name = argv[optind];
    printf("file %s\n", file_name);

    return 0;
}


static size_t read_file(char* file_name, unsigned char** data)
{
    *data = NULL;

    FILE* file = fopen(file_name, "rb");

    if (file == NULL)
    {
        fprintf(stderr, "cannot open in file %s\n", file_name);
        return 0;
    }

    fseek(file, 0, SEEK_END);

    long file_length = ftell(file);

    if (file_length < 0)
    {
        fprintf(stderr, "cannot get length of file %s\n", file_name);
        fclose(file);
        return 0;
    }

    *data = (unsigned char*)malloc(file_length);

    if (*data == NULL)
    {
        fprintf(stderr, "cannot allocate file_data buffer\n");
        fclose(file);
        return 0;
    }

    fseek(file, 0, SEEK_SET);

    size_t read_length = fread(*data, 1, file_length, file);

    fclose(file);

    if (read_length != (size_t)file_length)
    {
        fprintf(stderr, "wrong read length (%zi)\n", read_length);
        free(*data);
        return 0;
    }

    return read_length;
}


int main(int argc, char* argv[])
{
    if (parse_arguments(argc, argv) != 0)
    {
        return 1;
    }

    unsigned char* file_data = NULL;

    size_t file_length = read_file(file_name, &file_data);

    LibRaw rp;
    rp.imgdata.params.output_tiff = 1;
    rp.imgdata.params.user_qual = quality;

    int res;
    res = rp.open_bayer(file_data,
                        file_length,
                        width,
                        height,
                        0,
                        0,
                        0,
                        0,
                        0,
                        bayer_pattern,
                        0,
                        0,
                        black_level);

    if (res != LIBRAW_SUCCESS)
    {
        fprintf(stderr, "error opening bayer data\n");
        goto error_cleanup;
    }

    if ((res = rp.unpack()) != LIBRAW_SUCCESS)
    {
        fprintf(stderr, "unpack error: %s\n", libraw_strerror(res));
        goto error_cleanup;
    }

    if ((res = rp.dcraw_process()) != LIBRAW_SUCCESS)
    {
        fprintf(stderr, "processing error: %s\n", libraw_strerror(res));
        goto error_cleanup;
    }

    char file_name_out[256];

    sprintf(file_name_out, "%s.tif", file_name);

    res = rp.dcraw_ppm_tiff_writer(file_name_out);

    if (res == LIBRAW_SUCCESS)
    {
        printf("created %s\n", file_name_out);
    }
    else
    {
        fprintf(stderr,
                "cannot write %s: %s\n",
                file_name_out,
                libraw_strerror(res));
        goto error_cleanup;
    }

    return 0;

error_cleanup:
    free(file_data);
    return 1;
}
