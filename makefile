SRCS = demosaic.cpp
TARGET = demosaic

CPPFLAGS = -MMD
CXXFLAGS = -std=gnu++17 -O0 -ggdb3 -Wall -Wextra -Wpedantic
LDFLAGS = -LLibRaw/lib/.libs/
LDLIBS = -lraw

OBJS = $(SRCS:.cpp=.o)
DEPS = $(OBJS:.o=.d)

CC = g++

.PHONY: all
all: $(TARGET)

#$(TARGET): $(OBJS)
#	$(CC) $(LDFLAGS) $(OBJS) $(LDLIBS) -o $(TARGET)

.PHONY: libraw
libraw:
	cd LibRaw && ./configure && $(MAKE)

.PHONY: clean
clean:
	$(RM) test-input.raw.tif $(OBJS) $(DEPS) $(TARGET)

.PHONY: format
format:
	clang-format -i --style=file $(SRCS)

.PHONY: check
check:
	cppcheck --enable=all $(SRCS)

.PHONY: test
test: all
	./demosaic test-input.raw
	./demosaic -p gb -q 4 -b 128 -x 1024 -y 1024 test-input.raw

.PHONY: tags
tags:
	ctags -R .

-include $(DEPS)
