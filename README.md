# demosaic

demosaic is a command line program to [demosaic](https://en.wikipedia.org/wiki/Demosaicing) (aka debayer) image sensor data.

It is only a thin wrapper around [LibRaw](https://www.libraw.org/), so kudos goes to them. LibRaw is dual licensed under LGPL version 2.1 and CDDL version 1.0.

To build this project, first create a subfolder `LibRaw` with the latest LibRaw source code. Then cd into the demosaic directory, and

    make libraw
    make

The generated executable is `demosaic`.
Type `demosaic -h` for help on usage.

Example usage: `demosaic test-input.raw`.
